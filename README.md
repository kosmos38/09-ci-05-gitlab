# Домашнее задание к занятию "09.05 Gitlab"

Все роли пройдены успешно:

### DevOps

```
image: docker:latest
services:
  - docker:dind
stages:
  - build
  - deploy
build:
  stage: build
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker build -t "$CI_REGISTRY/kosmos38/09-ci-05-gitlab/image:latest" .
  except:
    - master
build&deploy:
  stage: deploy
  script:  
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker build -t "$CI_REGISTRY/kosmos38/09-ci-05-gitlab/python-api:latest" .
    - docker push "$CI_REGISTRY/kosmos38/09-ci-05-gitlab/python-api:latest"
  only:
    - master
```

### Product Owner

![alt text](screenshots/ProductOwner.png "ProductOwner")​

### Developer

![alt text](screenshots/Developer.png "Developer")​

### Tester

![alt text](screenshots/Tester.png "Tester")​